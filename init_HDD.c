#include<stdio.h>
#include<stdlib.h>
typedef unsigned short int MODE_T;
typedef unsigned int NLINK;
typedef unsigned int UINT32;
typedef unsigned short int UINT16;
typedef unsigned char UINT8;
typedef unsigned int SIZE_T;
typedef unsigned int TIME_T;
typedef int* INT_P;
typedef int long long INTLL;

struct Disk_INode 
{
	MODE_T 	 st_mode ; 	// 3-bits for file type & 9 bits for permissions 
	UINT32	 st_uid ;	// user id
	NLINK 	 st_link ;	// Numbers of links  
	//INT16	 st_gid ;	// group id 
	SIZE_T	 st_size ;	// size in bytes for regular file 
	TIME_T 	 at_time;	// File access time 
	TIME_T	 mt_time;	// Modification time 
	TIME_T   ct_time;	// I-node change time 
	INT_P    file_content[7];//pointer todisk block
				// 1st 6 are direct refrence and last one is single indirect  
	INTLL	 reserve;	// extra reserved byter to make inode in power of 2 
};

struct Blk_Read
{
char a[256]; 			// struct will get 256 bytes of memory 
};


struct SuperBlock
{
	UINT32 iFs_size;		// file system size
	UINT16 iFree_Blks;	// Number of free blocks
	UINT8  iList_Blk[4096];	// List of free blocks on file System
	UINT8  iFs_INode_size;	// Number of INodes free in FS
	UINT8  iList_INode[256];// List of free INodes on File system
};

//Array of free INode and Blocks is used instreat of Linklist. 
//Therefore index of next free inode and block is not added into superblock structure.

// Vertual HDD info. 
// Above Inode structure (struct stat) is of size 32 Bytes 
// Consider block size is 128 Bytes
//
char* HDD;
struct SuperBlock * pSuperBlk;	
struct Blk_Read * pBlk_INodeBlk ;		// Pointer to INode Block of size 16,384 Bytes (16KB), 256 Inodes , 64 Blks
struct Blk_Read * pBlk_DataBlk ;		// Pointer to Data Block of size 1,048,576 Bytes (1024 KB) , 4096 Blks
 
int main()
{	
	int HDDsize= (sizeof(struct SuperBlock))+sizeof(struct Blk_Read)*(4096+64);
	
	if(HDD =(char*)malloc(HDDsize))
	{
		printf("Memmory allocation Error");
	}
	pSuperBlk = (struct SuperBlock*)HDD;
	pBlk_INodeBlk = (struct Blk_read*)(HDD+(sizeof(struct SuperBlock)));		
	pBlk_DataBlk  =  pBlk_INodeBlk+64;
	

	

	/*
	printf("\n\n pointer SB = %u ",8*sizeof(struct SuperBlock));
	printf("\n\n Size of HDD = %ld ",sizeof(HDD));
	printf("\n\nsize of SuperBlock = %d ",sizeof(struct SuperBlock));

	printf("\n\nHDD =%u , INode = %u ,Data =%u \n\n\n",pSuperBlk, pBlk_INodeBlk ,pBlk_DataBlk);	
	*/
	free((void*)HDD);
	return 0;
}

